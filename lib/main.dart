import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;


// class MyAppBar extends StatelessWidget {
//   const MyAppBar({required this.title, Key? key}) : super(key: key);

//   final Widget title;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 56.0, 
//       padding: const EdgeInsets.symmetric(horizontal: 8.0),
//       decoration: BoxDecoration(color: Colors.blue[500]),
      
//       child: Row(
        
//         children: [
          
//           Expanded(
//             child: title,
//           ),
//           const IconButton(
//             icon: Icon(Icons.search),
//             tooltip: 'Search',
//             onPressed: null,
//           ),
//         ],
//       ),
//     );
//   }
// }

class MyScaffold extends StatelessWidget {
  const MyScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Life Style Buffet',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Restaurant')
          ),
          body: SafeArea(
            child: ListView(
              children: <Widget>[
                Container(height: 50,),
                Container( 
                  
                  child: 
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Image.asset('images/buffet1.jpg',width: 400,height: 200)],
                  ),
                ),Container(height: 50,),
                Container( 
                  
                  child: 
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Image.asset('images/buffet2.jpg',width: 400,height: 200)],
                  ),
                ),Container(height: 50,),
                
                Container(height: 50,),
              ],
            ),
          ),
        ));
  }
}
      
void main() {
  runApp(
    const MaterialApp(
      title: 'Life Style Buffet',
      home: SafeArea(
        child: MyScaffold(),
      ),
    ),
  );
}

